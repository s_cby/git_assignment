//This is a Factorial Class
import java.util.Scanner;
//The main class 
class Factorial
{
   public static void main(String args[])
   {
      int n, c, fact = 1;
 
      System.out.println("Enter an integer to calculate it's factorial");
      //out putting words
      Scanner in = new Scanner(System.in);
 
      n = in.nextInt();
 //getting inputs 
      if ( n < 0 )
         System.out.println("Number should be non-negative.");
      else
      {
//calculating 
         for ( c = 1 ; c <= n ; c++ )
            fact = fact*c;
 
         System.out.println("Factorial of "+n+" is = "+fact);
      }
   }
}
